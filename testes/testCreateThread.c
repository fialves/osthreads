#include <stdio.h>
#include <stdlib.h>
#include "../include/fila2.h"
#include "../include/cthread.h"

void imprime(void *x){
    printf("imprime is running.\n");
    printf("imprime arg: %d\n",*((int *)x));
    return;
}

int main(){
    int arg = 10;

    if(ccreate(imprime,(void *)&arg) !=0) return -1;
    arg++;
    if(ccreate(imprime,(void *)&arg) !=0) return -2;
    arg++;
    if(ccreate(imprime,(void *)&arg) !=0) return -3;
    cyield();
    printf("Thread criada com sucesso!\n");

    return 0;
}
