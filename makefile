all:  test1

libfila2:
	ar crs ./lib/libfila2.a ./bin/fila2.o

test1: libfila2
	gcc -o teste1 testes/testCreateThread.c src/cthread.c -Llib -lfila2 -Wall

test2: libfila2
	gcc -o teste2 testes/testJoinThread.c src/cthread.c -Llib -lfila2 -Wall

clean:
	rm -r lib/* teste1 teste2
