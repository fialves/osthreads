#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>
#include "../include/fila2.h"
#include "../include/cthread.h"
#include "../include/cdata.h"

#define OPERATION_YIELD 0
#define OPERATION_JOIN 1
#define OPERATION_WAIT 2
#define OPERATION_FINISH 3

#define FIRST_TID 0

#define SUCCESS 0
#define ERROR_EMPTY_QUEUE -2
#define ERROR_RUNNING_SOMETHING -1

FILA2 readyQueue;
FILA2 blockQueue;
int lastTID = FIRST_TID;
char stack[SIGSTKSZ];

ucontext_t  *finishContext;
TCB_t       *executingThread;
TCB_t       *nextThread;

void scheduler(int operation){
    switch(operation){
        case OPERATION_JOIN:
            printf("scheduler is running!\n"); // DEBUG
            executingThread->state = PROCST_BLOQ;		
	    if(AppendFila2(&blockQueue,executingThread) != SUCCESS){
		printf("[CJOIN] Erro ao colocar na fila de bloqueados.\n");
		return;
	    }
            break;
        case OPERATION_WAIT:
            executingThread->state = PROCST_BLOQ;
            // TODO: efeitos de wait
            break;
        case OPERATION_YIELD:
            printf("scheduler is running!\n"); // DEBUG
            executingThread->state = PROCST_APTO;
            if(AppendFila2(&readyQueue,executingThread) != SUCCESS)
		printf("[YIELD] Erro ao colocar na fila de aptos.\n");
            break;
        case OPERATION_FINISH:
            printf("scheduler is running!\n"); // DEBUG
            // TODO: efeitos de término da thread
            break;
    }

    dispatch();
}

int dispatch(){
    printf("dispatcher is running!\n"); // DEBUG
    if(isExecuting() != SUCCESS) return ERROR_RUNNING_SOMETHING;
    if(hasNodeFila2(&readyQueue) != SUCCESS) return ERROR_EMPTY_QUEUE;

    FirstFila2(&readyQueue);
    nextThread = (TCB_t*)GetAtIteratorFila2(&readyQueue);
    DeleteAtIteratorFila2(&readyQueue);

    TCB_t* prevThread = executingThread;
    executingThread = nextThread;

    printf(" ====== == = Thread is dispatched = == ======\n");
    printf("TID: %d\n",nextThread->tid);

    //setcontext(&(executingThread->context));
    swapcontext(&(prevThread->context),&(nextThread->context));
    printf("[DISPATCH] Dispatch finished.\n");
    return SUCCESS;
}

int cyield(void){
    printf("Cyield is running!\n");
    scheduler(OPERATION_YIELD);
    return SUCCESS;
}

void cfinish(int tid){
    printf("Cfinish is running!\n");
    printf("[CFINISH] Finishing TID %d(next:%d)\n",executingThread->tid,tid);
    if(tid > -1){
	    FirstFila2(&blockQueue);
	    TCB_t* checkThread = (TCB_t*)GetAtIteratorFila2(&blockQueue);
	    while(checkThread != NULL){
		if(checkThread->tid == tid){
			printf("[CFINISH] TID encontrado(tid=%d).\n",checkThread->tid);		
			DeleteAtIteratorFila2(&blockQueue);
			checkThread->state = PROCST_EXEC;
			executingThread = checkThread;
			setcontext(&(checkThread->context));
			printf("[CFINISH] Finished.\n");
			return;
		}
		printf("[CFINISH] PID ~ainda~ não encontrado.\n");		
		NextFila2(&blockQueue);
		checkThread = (TCB_t*)GetAtIteratorFila2(&blockQueue);
	    }
	    FirstFila2(&blockQueue);
	    printf("[CFINISH] PID não existe.\n");
	    return;
    }

    //free(executingThread->context.uc_stack.ss_sp);
    //free(&(executingThread->context));
    //free(executingThread);
    //executingThread = NULL;
    executingThread->state = PROCST_TERMINO;
    scheduler(OPERATION_FINISH);
}

int cjoin(int tid){
    FirstFila2(&readyQueue);
    TCB_t* checkThread = (TCB_t*)GetAtIteratorFila2(&readyQueue);
    while(checkThread != NULL){
	if(checkThread->tid == tid){
		printf("[CJOIN] TID encontrado(tid=%d).\n",checkThread->tid);		
		if(checkThread->context.uc_link != finishContext){
			printf("[CJOIN] TID já está sendo esperado.\n");
			FirstFila2(&readyQueue);
			return -1;
		}	

		makecontext(checkThread->context.uc_link,(void (*)(void))cfinish,1,executingThread->tid);	
		scheduler(OPERATION_JOIN);
		printf("[CJOIN] Finished.\n");
		return 0;
	}
	printf("[CJOIN] PID ~ainda~ não encontrado.\n");		
	NextFila2(&readyQueue);
	checkThread = (TCB_t*)GetAtIteratorFila2(&readyQueue);
    }
    FirstFila2(&readyQueue);
    printf("[CJOIN] PID não existe.\n");
    return -2;
}

int csem_init(csem_t *sem){
    return SUCCESS;
}

int cwait(csem_t *sem){
    return SUCCESS;
}

int csignal(csem_t *sem){
    return SUCCESS;
}

int init_finishContext(){
    finishContext = (ucontext_t*)malloc(sizeof(ucontext_t));
    if(finishContext == NULL) {
        printf("[CCREATE] Finish context is empty.\n");
        return -1;
    }
    finishContext->uc_stack.ss_sp = (char *) malloc(SIGSTKSZ);
    finishContext->uc_stack.ss_size = SIGSTKSZ;
    finishContext->uc_link = NULL;
    getcontext(finishContext);
    makecontext(finishContext,(void (*)(void))cfinish,1,-1);
    return 0;
}

int init_mainthread(){
    TCB_t* newTCBforMain = (TCB_t*) malloc(sizeof(TCB_t));
    if(newTCBforMain == NULL) {
        printf("[CCREATE] Main thread is empty.\n");
        return -1;
    }    
    newTCBforMain->tid = lastTID;
    newTCBforMain->state = PROCST_EXEC;
    getcontext(&(newTCBforMain->context));
    newTCBforMain->context.uc_stack.ss_sp = stack;
    newTCBforMain->context.uc_stack.ss_size = sizeof(stack);
    newTCBforMain->context.uc_link = NULL;
    executingThread = newTCBforMain;
    return 0;
}

int init_thread(void* (*start)(void*), void *arg){
    lastTID++;
    TCB_t* newTCB = (TCB_t*) malloc(sizeof(TCB_t));
    if(newTCB == NULL) {
        printf("[CCREATE] New Thread is empty.\n");
        return -1;
    }
    getcontext(&(newTCB->context));
    newTCB->tid = lastTID;
    newTCB->state = PROCST_CRIACAO;
    newTCB->context.uc_stack.ss_sp = malloc(SIGSTKSZ);
    newTCB->context.uc_stack.ss_size = SIGSTKSZ;
    newTCB->context.uc_link = finishContext;

    makecontext(&(newTCB->context),(void (*)(void))start,1,arg);

    if(AppendFila2(&readyQueue,newTCB) != SUCCESS) {
        printf("[CCREATE] Erro ao inserir thread na fila.\n");
        return -1;
    }

    // DEBUG
    printf(" ====== == = Thread is created = == ======\n");
    printf("TID: %d\n",newTCB->tid);
    return 0;
}

int ccreate (void* (*start)(void*), void *arg){
    if(lastTID == FIRST_TID){
        if(CreateFila2(&readyQueue)!=0){
            printf("[CCREATE] Erro ao criar a fila.\n");
            return -1;
        }
	if(CreateFila2(&blockQueue)!=0){
            printf("[CCREATE] Erro ao criar a fila.\n");
            return -1;
        }
	init_finishContext();
        init_mainthread();
    }

    init_thread(start,arg);

    return SUCCESS;
}

int isExecuting(){
    if(executingThread->state == PROCST_EXEC){
        printf("[DISPATCHER] Thread ainda em execução.\n");
        return ERROR_RUNNING_SOMETHING;
    }
    return SUCCESS;
}

int hasNodeFila2(PFILA2 fila){
    LastFila2(fila);
    if(GetAtIteratorFila2(fila) == NULL){
        printf("[DISPATCHER] Fila vazia.\n");
        return ERROR_EMPTY_QUEUE;
    }
    return SUCCESS;
}
